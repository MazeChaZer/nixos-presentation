$latex = 'latex -interaction=nonstopmode -shell-escape';
$pdflatex = 'pdflatex -interaction=nonstopmode -shell-escape';
$pdf_mode = 1;
@default_files = ('presentation.tex', 'handout.tex');
